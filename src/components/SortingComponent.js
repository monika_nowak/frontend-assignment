import React from 'react';
import { Button, Typography, Box } from '@material-ui/core';

export default function SortingComponent(props) {

    return (
        <Box display="flex" justifyContent="flex-end" pb={5}>
            <Box textAlign="center">
                <Typography variant="h6"><u>Sort by date</u></Typography>
                <Button variant="contained"onClick={() => props.sort()}>Sort</Button>
            </Box>
        </Box>
    )
}