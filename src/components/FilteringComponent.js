import React from 'react';
import { Typography, Checkbox, Container, Box } from '@material-ui/core';

export default function FilteringComponent(props) {

    return (
        <Container className="filter">
            <Box pl={{ xs: '30px', md: '10px' }}>
                <Typography variant="h6"><u>Data sources</u></Typography>
            </Box>
            <Box >
                <Box display={{ xs: 'inline', md: 'block' }}>
                    <Checkbox color="primary" type="checkbox" defaultChecked onChange={(event) => { if (event.target.checked) props.check("fashion"); else props.uncheck("fashion"); }} />
                    <Typography display="inline">Fashion</Typography>
                </Box>
                <Box display={{ xs: 'inline', md: 'block' }}>
                    <Checkbox color="primary" type="checkbox" onChange={(event) => { if (event.target.checked) props.check("sport"); else props.uncheck("sport"); }} />
                    <Typography display="inline">Sport</Typography>
                </Box>
            </Box>
        </Container>
    )
}
