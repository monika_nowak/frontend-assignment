import React, { Component } from 'react';
import './ArticleComponent.css';
import { ListItem, ListItemText, Typography } from '@material-ui/core';

export default class ArticleComponent extends Component {

  primaryText() {
    return (<React.Fragment>
      <Typography className="primary" variant="h6">{this.props.article.title}</Typography>
      <Typography className="primary" variant="subtitle2">{this.props.article.date}</Typography>
    </React.Fragment>)
  }

  secondaryText() {
    return (
      <Typography className="secondary" variant="body1" align="justify" color="textPrimary">{this.props.article.preamble}</Typography>
    )
  }

  render() {
    const imgPath = this.props.article.image;
    return (
      <ListItem className="article">
        <img className="img" src={imgPath} alt="Missing data." />
        <ListItemText className="article-text-item"
          primary={this.primaryText()}
          secondary={this.secondaryText()} >
        </ListItemText>
      </ListItem >
    )
  }
}
