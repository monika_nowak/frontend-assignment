import React, { Component } from 'react';
import './App.css';
import ArticleComponent from './components/ArticleComponent';
import FilteringComponent from './components/FilteringComponent';
import SortingComponent from './components/SortingComponent';
import parse from 'date-fns/parse';
import nbLocale from 'date-fns/locale/nb';
import { List, Grid, Container, Typography } from '@material-ui/core';

export default class App extends Component {

  constructor() {
    super();
    this.state = {
      articles: [],
      checkedCategory: ["fashion"],
      sortAsc: undefined,
      errors: []
    }

    this.check = this.check.bind(this);
    this.uncheck = this.uncheck.bind(this);
    this.filterArticles = this.filterArticles.bind(this);
    this.sort = this.sort.bind(this);
  }

  componentDidMount() {
    this.fetchArticles("sports");
    this.fetchArticles("fashion");
  }

  fetchArticles(category) {
    fetch(`/articles/${category}`).then(async res => {
      if (res.status === 200) {
        const json = await res.json();
        this.setState(prevState => ({ articles: prevState.articles.concat(json.articles) }));
      } else {
        this.setState(prevState => ({ errors: prevState.errors.concat(`Error loading ${category} articles.`) }));
      }
    });
  }

  filterArticles(keyword) {
    return this.state.articles.filter(article => {
      return article.category !== keyword
    });
  }

  check(keyword) {
    this.setState({ checkedCategory: this.state.checkedCategory.concat(keyword) });
  }

  uncheck(keyword) {
    this.setState({ checkedCategory: this.state.checkedCategory.filter(category => category !== keyword) })
  }

  getDate(dateString) {
    return parse(dateString, 'd. MMMM yyyy', new Date(), { locale: nbLocale });
  }

  sort() {
    this.setState({ sortAsc: !this.state.sortAsc });
  }

  getSorted(articles) {
    if (this.state.sortAsc === undefined) {
      return articles;
    }
    else if (this.state.sortAsc) {
      return articles.sort((article, article2) =>
        this.getDate(article.date) - this.getDate(article2.date));
    }
    else {
      return articles.sort((article, article2) =>
        this.getDate(article2.date) - this.getDate(article.date));
    }
  }

  getArticles() {
    const filtered = this.state.articles.filter(article => this.state.checkedCategory.includes(article.category));
    return this.getSorted(filtered);
  }

  renderList(articles) {
    if (articles.length > 0) {
      return (
        <List className="articles-list">
          {articles.map(article =>
            (<ArticleComponent key={article.id} article={article}> </ArticleComponent>))}
        </List>);
    }
    else {
      return (<Typography variant="h4">No articles to display.</Typography>)
    }
  }

  renderErrors(errors) {
    return errors.map(error => (<div className="error">{error}</div>));
  }

  render() {
    const articles = this.getArticles();
    const errors = this.state.errors;
    return (
      <Container >
        <Grid container justify="center" >
          {this.renderErrors(errors)}
          <Grid item sm={6} md={12}>
            <SortingComponent sort={this.sort}></SortingComponent>
          </Grid>
          <Grid item sm={6} md={3}>
            <FilteringComponent check={this.check} uncheck={this.uncheck} />
          </Grid>
          <Grid item md={9}>
            {this.renderList(articles)}
          </Grid>
        </Grid>
      </Container>
    );
  }
}
